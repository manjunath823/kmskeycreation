#Terraform settings block to mention the provider and its version constrians
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.73.0"
    }
  }
}

#Defining the provider block with default region
provider "aws" {
  profile = "default"
  region = "us-east-1"
}

#caller identity data resource block to get the executing user details arn
data "aws_caller_identity" "current" {}

#To get the existing role created for SSO authentication to grant access for Key managment
data "aws_iam_roles" "example" {
  name_regex = "AWSReservedSSO_AWSAdministratorAccess*"
}


#Policy document includes service and AWS identifiers
data "aws_iam_policy_document" "ssm_key" {
  statement {
    sid = "Allow CloudTrail and AWS Config to encrypt/decrypt logs"
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "cloudtrail.amazonaws.com",
        "config.amazonaws.com"
      ]
    }
    actions = [
      "kms:GenerateDataKey",
      "kms:Decrypt"
    ]
    resources = ["*"]
  }
  statement {
    sid       = "Allow access for Key Administrators"
    actions   = [
      "kms:Create*",
      "kms:Describe*",
      "kms:Enable*",
      "kms:List*",
      "kms:Put*",
      "kms:Update*",
      "kms:Revoke*",
      "kms:Disable*",
      "kms:Get*",
      "kms:Delete*",
      "kms:TagResource",
      "kms:UntagResource",
      "kms:ScheduleKeyDeletion",
      "kms:CancelKeyDeletion"
    ]
    resources = ["*"]

    principals {
      type = "AWS"
      identifiers = [ "${join(", ", [for s in data.aws_iam_roles.example.arns : s])}", "${data.aws_caller_identity.current.arn}" ]
      #Iterating over dynamic data resource to convert list of strings to string element
      #Also adding caller identity to make the user authorised to manage KMS key
    }
  }
}

#KMS key resource block with default encryption type as Control tower will only supports Symmetric key type
resource "aws_kms_key" "control_tower_key" {
  description             = "KMS Key for AWS Control tower"
  #Symmetric key type SYMMETRIC_DEFAULT which is AES 256 encryption type
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  policy = data.aws_iam_policy_document.ssm_key.json
  is_enabled               = true
  #Specifies the intended use of the key
  key_usage                = "ENCRYPT_DECRYPT"
  #Default it will be 7 days and can be maximum 30 days
  deletion_window_in_days  = 7
  #AWS Control Tower KMS only supports single region type, hence setting multi region argument false
  multi_region             = false
}

#Creating alias as a name identity for the KMS key
resource "aws_kms_alias" "control_tower_kms_alias" {
  name          = "alias/AWS_Control_Tower_KMS_Key"
  target_key_id = aws_kms_key.control_tower_key.key_id
}

#Sample output value for the fetched SSO administrationAccess roles
output "sample_output" {
  value = data.aws_iam_roles.example.names
  
}

#Sample output for the caller identity values
output "caller_identity" {
  value = data.aws_caller_identity.current.arn
}