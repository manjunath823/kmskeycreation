KMS Key Creation for encrypting AWS Control tower resources data
=================================================================

* AWS Control Tower that provides us the option to use a single customer provided AWS Key Management Service (AWS KMS) key to secure the AWS Control Tower deployed services (AWS CloudTrail, AWS Config). 
* To configure this functionality, you can select KMS Key Configuration during your initial landing zone setup or you can perform a landing zone update to access this selection for an existing AWS Control Tower landing zone.

Important Notes
===============
1. Key must be Symmetric and located in the same region as Control Tower Home Region. As AWS Control Tower does not support multi-Region keys or asymmetric keys and Key will be generated with symmetric key type "SYMMETRIC_DEFAULT"

2. It requires access to AWS Config and Cloudtrail Services. To make sure the minimum permissions assigned to KMS key, blow policy will be added
```
{
  "Sid": "Allow CloudTrail and AWS Config to encrypt/decrypt logs",
  "Effect": "Allow",
  "Principal": {
    "Service": [
      "cloudtrail.amazonaws.com",
      "config.amazonaws.com"
   ]
  },
   "Action": [
      "kms:GenerateDataKey",
      "kms:Decrypt"
   ],
    "Resource": "*"
}
```

***When you select Set up landing zone, AWS Control Tower performs a pre-check to validate your KMS key.***

The key must meet these requirements:
===================================
* Enabled
* Symmetric
* Not a multi-Region key
* Has correct permissions added to the policy
* Key is in the management account

We may see an error banner if the key does not meet these requirements.


Inputs:
========
* Enabling the KMS key
```
variable "is_enabled_flag" {
  description = "true for enabling the KMS key"
  type        = bool
  default     = true
}
```
KMS Key must set to enable to attach with AWS control tower


* Deletion window time in days
``` 
variable "deletion_window_days" {
  type        = number
  default     = 7
  description = "Duration in days after which the key is deleted after destruction of the resource"
}
```
The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. The value must be between 7 and 30, inclusive. If we do not specify a value, it defaults to 30

* Key cipher spec
```
variable "customer_master_key_spec" {
  type        = string
  default     = "SYMMETRIC_DEFAULT"
  description = "Specifies whether the key contains a symmetric key or an asymmetric key pair and the encryption algorithms or signing algorithms that the key supports. Valid values: `SYMMETRIC_DEFAULT`, `RSA_2048`, `RSA_3072`, `RSA_4096`, `ECC_NIST_P256`, `ECC_NIST_P384`, `ECC_NIST_P521`, or `ECC_SECG_P256K1`."
}
```
As AWS Control tower only supports Symmetric key type the value must be set and defaults to "SYMMETRIC_DEFAULT"

* Multi regions KMS Key
```
variable "multi_region" {
  type        = bool
  default     = false
  description = "Indicates whether the KMS key is a multi-Region (true) or regional (false) key."
}
```
As AWS Control Tower only supports single region type and it default set it to false

* Key usage
```
variable "key_usage" {
  type        = string
  default     = "ENCRYPT_DECRYPT"
  description = "Specifies the intended use of the key. Valid values: `ENCRYPT_DECRYPT` or `SIGN_VERIFY`."
}
```
Specifies the intended use of the key


Reference:
https://docs.aws.amazon.com/controltower/latest/userguide/best-practices.html